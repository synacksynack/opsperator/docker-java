#!/bin/sh

JAVA_TRUST_STORE="${JAVA_TRUST_STORE:-/etc/ssl/certs/java/cacerts}"
JAVA_TRUST_STORE_PASS="${JAVA_TRUST_STORE_PASS:-changeit}"
RESET_TLS=${RESET_TLS:-false}
if test "$RESET_TLS" = true; then
    rm -f "$JAVA_TRUST_STORE"
fi
if ! test -s "$JAVA_TRUST_STORE"; then
    echo Provision Keystore
    if ! test -d "$(dirname "$JAVA_TRUST_STORE")"; then
	mkdir -p $(dirname "$JAVA_TRUST_STORE")
    fi
    count=0
    for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
    do
	if ! test -s $f; then
	    continue
	fi
	old=0
	grep -n 'END CERTIFICATE' "$f"  | awk -F: '{print $1}' \
	    | while read stop
	    do
		count=`expr $count + 1`
		echo "Processing $f (#$count)"
		head -$stop "$f" \
		    | tail -`expr $stop - $old` >/tmp/insert.crt
		keytool -import -trustcacerts -alias inter$count \
		    -file /tmp/insert.crt -keystore "$JAVA_TRUST_STORE" \
		    -storepass "$JAVA_TRUST_STORE_PASS" -noprompt
		old=$stop
		d=kube-$count`echo $f | sed 's|/|-|g'`
		cat /tmp/insert.crt >/usr/local/share/ca-certificates/$d
	    done
	echo done with "$f"
    done
    if test -s /tmp/insert.crt; then
	update-ca-certificates
    fi
    rm -f /tmp/insert.crt
    unset count old
fi
