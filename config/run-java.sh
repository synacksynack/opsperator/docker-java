#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/nsswrapper.sh
. /usr/local/bin/reset-tls.sh

if test -z "$@"; then
    if test -z "$JAR_FILE"; then
	JAR_FILE=$(find /usr/src/ -type f -name '*.jar' | head -1)
    fi
    if test "$JAR_FILE"; then
	set -- java -jar "$JAR_FILE"
    fi
fi

if test -z "$@"; then
    cat <<EOF
Image can be used as a base building docker images, or shipping
applications. Place your jar into /usr/src, or set a JAR_FILE env
var pointing to the right place.
EOF
    sleep 120
    exit 0
fi

echo "Starting $@"
exec "$@"
