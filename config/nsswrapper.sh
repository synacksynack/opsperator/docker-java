#!/bin/sh

if test "`id -u`" -ne 0; then
    RUNTIME_USER=${RUNTIME_USER:-java}
    RUNTIME_GROUP=${RUNTIME_GROUP:-$RUNTIME_USER}
    RUNTIME_HOME=${RUNTIME_HOME:-/tmp}
    RUNTIME_SHELL=${RUNTIME_SHELL:-/usr/sbin/nologin}
    if test -s /tmp/java-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to $RUNTIME_GROUP
	(
	    grep -v ^$RUNTIME_USER /etc/passwd
	    echo "$RUNTIME_USER:x:`id -u`:`id -g`:$RUNTIME_USER:$RUNTIME_HOME:$RUNTIME_SHELL"
	) >/tmp/java-passwd
	(
	    grep -v ^$RUNTIME_GROUP /etc/group
	    echo "$RUNTIME_GROUP:x:`id -g`:"
	) >/tmp/java-group
    fi
    export NSS_WRAPPER_PASSWD=/tmp/java-passwd
    export NSS_WRAPPER_GROUP=/tmp/java-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
