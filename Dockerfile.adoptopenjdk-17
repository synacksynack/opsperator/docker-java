FROM docker.io/debian:buster-slim

# Java Base image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    JRE_URL=https://github.com/AdoptOpenJDK \
    JRE_BUILD=2021-05-07-13-31 \
    HOTSPOT_BUILD=2021-05-06-23-30 \
    JRE_MAJOR=17
#https://github.com/AdoptOpenJDK/openjdk17-binaries/releases/download/jdk-2021-05-07-13-31/OpenJDK-jdk_x64_linux_hotspot_2021-05-06-23-30.tar.gz

LABEL io.k8s.description="JRE $VERSION Base Image" \
      io.k8s.display-name="AdoptOpenJDK $VERSION" \
      io.openshift.tags="java" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-java" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$VERSION"

COPY config/* /
ENV JAVA_HOME=/opt/jdk

RUN set -x \
    && echo 'Acquire::ForceIPv4 "true";' | tee /etc/apt/apt.conf.d/99force-ipv4 \
    && mv /nsswrapper.sh /usr/local/bin/ \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Installing JRE $VERSION Dependencies" \
    && apt-get install -y --no-install-recommends unzip libnss-wrapper wget \
	ca-certificates p11-kit \
    && echo "# Installing JRE $VERSION" \
    && mkdir /opt/jdk \
    && JKARCH=`uname -m` \
    && if ! echo "$JKARCH" | grep -E '(x86_64|aarch64)' >/dev/null; then \
	echo CRITICAL: unsupported architecture \
	&& exit 1; \
    elif test "$JKARCH" = x86_64; then \
	JKARCH=x64; \
    fi \
    && wget --no-check-certificate \
	$JRE_URL/openjdk${JRE_MAJOR}-binaries/releases/download/jdk-$JRE_BUILD/OpenJDK-jdk_${JKARCH}_linux_hotspot_$HOTSPOT_BUILD.tar.gz \
	-O- | tar -C /opt/jdk --strip-components 1 -xzf - \
    && find /opt/jdk/bin -type f | while read bin; \
	do \
	    ln -sf $bin /usr/bin/; \
	done \
    && find /opt/jdk/lib -name '*.so' -exec dirname '{}' ';' \
	| sort -u >/etc/ld.so.conf.d/docker-openjdk.conf \
    && ldconfig \
    && ( \
	echo '#!/usr/bin/env bash'; \
	echo 'set -Eeuo pipefail'; \
	echo 'if ! [ -d "$JAVA_HOME" ]; then'; \
	echo '    echo >&2 "error: missing JAVA_HOME environment variable"'; \
	echo '    exit 1'; \
	echo fi; \
	echo 'cacertsFile=; for f in "$JAVA_HOME/lib/security/cacerts" "$JAVA_HOME/jre/lib/security/cacerts"; do if [ -e "$f" ]; then cacertsFile="$f"; break; fi; done'; \
	echo 'if [ -z "$cacertsFile" ] || ! [ -f "$cacertsFile" ]; then'; \
	echo '    echo >&2 "error: failed to find cacerts file in $JAVA_HOME"'; \
	echo '    exit 1'; \
	echo fi; \
	echo 'trust extract --overwrite --format=java-cacerts --filter=ca-anchors --purpose=server-auth "$cacertsFile"'; \
    ) >/etc/ca-certificates/update.d/docker-openjdk \
    && chmod +x /etc/ca-certificates/update.d/docker-openjdk \
    && /etc/ca-certificates/update.d/docker-openjdk \
    && mv /reset-tls.sh /usr/local/bin \
    && echo "# Fixing Permissions" \
    && for d in /tmp /usr/src /etc/ssl /usr/local/share/ca-certificates \
	$JAVA_HOME/lib/security; \
	do \
	    chown -R 1001:root $d \
	    && chmod -R g=u $d; \
	done \
    && echo "# Cleaning Up" \
    && apt-get remove -y --purge wget \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
WORKDIR /usr/src
ENTRYPOINT ["dumb-init","--","/run-java.sh"]
