FROM docker.io/debian:buster-slim

# Java Base image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    JRE_MAGIC=51f4f36ad4ef43e39d0dfdbaf6549e32/9 \
    JRE_URL=https://download.java.net/java/GA/jdk \
    VERSION=15.0.1

LABEL io.k8s.description="JRE $VERSION Base Image" \
      io.k8s.display-name="Oracle Java JRE $VERSION" \
      io.openshift.tags="java" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-java" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$VERSION"

COPY config/* /
ENV JAVA_HOME=/opt/jdk-$VERSION

RUN set -x \
    && echo 'Acquire::ForceIPv4 "true";' | tee /etc/apt/apt.conf.d/99force-ipv4 \
    && mv /nsswrapper.sh /usr/local/bin/ \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Installing JRE $VERSION Dependencies" \
    && apt-get install -y --no-install-recommends unzip libnss-wrapper wget \
	ca-certificates p11-kit \
    && echo "# Installing JRE $VERSION" \
    && JKARCH=`uname -m` \
    && if ! echo "$JKARCH" | grep -E '(x86_64|aarch64)' >/dev/null; then \
	echo CRITICAL: unsupported architecture \
	&& exit 1; \
    elif test "$JKARCH" = x86_64; then \
	JKARCH=x64; \
    fi \
    && wget --no-check-certificate \
	$JRE_URL$VERSION/$JRE_MAGIC/GPL/openjdk-${VERSION}_linux-${JKARCH}_bin.tar.gz \
	-O- | tar -C /opt -xzf - \
    && find /opt/jdk-$VERSION/bin -type f | while read bin; \
	do \
	    ln -sf $bin /usr/bin/; \
	done \
    && find /opt/jdk-$VERSION/lib -name '*.so' -exec dirname '{}' ';' \
	| sort -u >/etc/ld.so.conf.d/docker-openjdk.conf \
    && ldconfig \
    && ( \
	echo '#!/usr/bin/env bash'; \
	echo 'set -Eeuo pipefail'; \
	echo 'if ! [ -d "$JAVA_HOME" ]; then'; \
	echo '    echo >&2 "error: missing JAVA_HOME environment variable"'; \
	echo '    exit 1'; \
	echo fi; \
	echo 'cacertsFile=; for f in "$JAVA_HOME/lib/security/cacerts" "$JAVA_HOME/jre/lib/security/cacerts"; do if [ -e "$f" ]; then cacertsFile="$f"; break; fi; done'; \
	echo 'if [ -z "$cacertsFile" ] || ! [ -f "$cacertsFile" ]; then'; \
	echo '    echo >&2 "error: failed to find cacerts file in $JAVA_HOME"'; \
	echo '    exit 1'; \
	echo fi; \
	echo 'trust extract --overwrite --format=java-cacerts --filter=ca-anchors --purpose=server-auth "$cacertsFile"'; \
    ) >/etc/ca-certificates/update.d/docker-openjdk \
    && chmod +x /etc/ca-certificates/update.d/docker-openjdk \
    && /etc/ca-certificates/update.d/docker-openjdk \
    && mv /reset-tls.sh /usr/local/bin \
    && echo "# Fixing Permissions" \
    && chown -R 1001:root /tmp /usr/src /etc/ssl /usr/local/share/ca-certificates \
    && chmod -R g=u /tmp /usr/src /etc/ssl /usr/local/share/ca-certificates  \
    && echo "# Cleaning Up" \
    && apt-get remove -y --purge wget \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
WORKDIR /usr/src
ENTRYPOINT ["dumb-init","--","/run-java.sh"]
